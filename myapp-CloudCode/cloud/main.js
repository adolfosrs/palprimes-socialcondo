
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.job("calculateAndSaveAllPalPrimes", function(request, response) {
  var arePrime = [];

  n = 10000000;

  for (var i=0; i<=n; i++){
  	 //first im assuming that all numbers are primes
     arePrime[i] = 1;
  }

  //zero and 1 are not prime
  arePrime[0] = 0;
  arePrime[1] = 0;

  //now for each number I setted as prime Im removing its multiple. 
  for (i=2; i<=n; i++){
  	if (arePrime[i] == 1){
  		for (var j=2;i*j<=n;j++){
  			arePrime[i*j] = 0;
  		}
  	}
  }

  var PalPrime = Parse.Object.extend("PalPrime");

  var palPrimesToSave = [];

  for (i=0; i<=n;i++){
  	if ((arePrime[i] == 1) && (i.toString() == i.toString().split('').reverse().join(''))){
  		var newPalPrime = new PalPrime();
  		newPalPrime.set("value",i);
  		palPrimesToSave.push(newPalPrime);
  	}
  }

  Parse.Object.saveAll(palPrimesToSave, {
    success: function(list) {
      response.success("Hello world!");
      // All the objects were saved.
    },
    error: function(error) {
      response.error(error)
      // An error occurred while saving one of the objects.
    },
  });

});


Parse.Cloud.define("getPrimesInInterval", function(request, response) {

});