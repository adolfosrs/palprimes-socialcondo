#!/usr/bin/env python

import os
import sys
import time

def calculatePalPrimes():

    n = 1003002
    arePrime = [1] * (n+1)

    arePrime[0] = 0
    arePrime[1] = 0

    for i in range(0,n):
        if arePrime[i] == 1:
            j = 2
            while i*j <= n:
                arePrime[i*j] = 0
                j+=1

    actualPrimesAndPal = []
    for i in range(0,n):
        if (arePrime[i]==1) and (str(i) == str(i)[::-1]):
            actualPrimesAndPal.append(i)

    return actualPrimesAndPal


def main():

    intInput = 0

    if len(sys.argv) != 2:
        print "give me 1 argument (1<=int<=1000000)"
        return 0
    
    else: 
        try:
            intInput = int(sys.argv[1])
        except:
            print "your arg must be an integer"
            return 0

    if not int(sys.argv[1])>=1 and int(sys.argv[1])<=1000000:
        print "integer between 1 and 1000000, please"

    else:

        #version 1
        start_version2_time = time.time()
        palPrimes = calculatePalPrimes()

        if intInput > palPrimes[len(palPrimes)-1]:
            print "Sorry, we couldnt find any palindromic prime bigger or equal to the value you entered."

        else:
            for i in range(0,len(palPrimes)):
                if intInput <= palPrimes[i]:
                    print palPrimes[i]
                    break

        print "Execution Time: " + str(time.time() - start_version2_time)
        #0.61 seconds

if __name__ == '__main__':
    main()
