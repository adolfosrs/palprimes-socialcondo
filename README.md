This is a simple application that aims to solve a programming problem that was presented on Social Condo’s internship selection process.

The problem given was as follows: “given an integer N where 1<=N<=1million, find the nearest greater value that is a palindromic prime inside the interval [1:1million].”

I developed two applications, one a simple python script, and the other an app for better presentation of the results.

**FIRST: PYTHON - Simple and Local solution.**

This is a simple solution based on Sieve of Eratosthenes to calculate the primes. After this process, I simply select the primes that are palindromic with a python trick. This solution takes the integer as an argument to execute and takes an average of 0.7 secs to calculate.


```
#!shell

python socialCondo-Adolfo.py N
```


**SECOND: iOS app with cloud computation**

After developing and validating the first solution, I started looking into optimisations so I could improve performance and provide a better experience for the user. With this approach, I was able to increase the available interval to 10 million maintaining a reasonable execution time.
For this approach, I used Parse to calculate the palindromic primes using Parse Cloud Code - javascript with some backbone elements. The results were saved into Parse back-end service.
After this, I made a simple iOS application to query the values on Parse database.

App demo: [https://www.youtube.com/watch?v=HKogriQrEFk](Link URL)