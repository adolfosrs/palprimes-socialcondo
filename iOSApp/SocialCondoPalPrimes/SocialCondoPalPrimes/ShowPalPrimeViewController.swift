//
//  ViewController.swift
//  SocialCondoPalPrimes
//
//  Created by Adolfo on 1/15/16.
//  Copyright © 2016 Spatz Solutions. All rights reserved.
//

import UIKit
import Parse

class ShowPalPrimeViewController: UIViewController {

    
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.messageLabel.alpha = 0.0
        self.resultLabel.alpha = 0.0
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func findPalPrime(sender: AnyObject) {
        
        if inputTextField.text!.isEmpty {
            self.messageLabel.text = "You need to enter a value :)"
            self.messageLabel.alpha = 1.0
            self.resultLabel.alpha = 0.0
        } else{
            
            if let inputInt = Int(inputTextField.text!){
                
                if !((inputInt>=1) && (inputInt<=10000000)){
                    self.messageLabel.text = "You must enter an integer between 1 and 10million!"
                    self.messageLabel.alpha = 1.0
                    self.resultLabel.alpha = 0.0
                } else {
                    
                    let query = PFQuery(className:"PalPrime")
                    query.whereKey("value", greaterThanOrEqualTo: inputInt)
                    query.orderByAscending("value")
                    query.findObjectsInBackgroundWithBlock {
                        (objects, error) -> Void in
                        if (error == nil) {
                            if (objects?.count == 0){
                                self.messageLabel.text = "Sorry, we couldnt find any palindromic prime bigger or equal to the value you entered :("
                                self.messageLabel.alpha = 1.0
                                self.resultLabel.alpha = 0.0
                            } else {
                                self.resultLabel.text = objects![0]["value"].stringValue
                                self.messageLabel.text = "We found one!"
                                self.messageLabel.alpha = 1.0
                                self.resultLabel.alpha = 1.0
                            }
                        }else {
                            print(error?.userInfo)
                        }
                    }
                }
                
                
            } else{
                self.messageLabel.text = "You must enter an integer!"
                self.messageLabel.alpha = 1.0
                self.resultLabel.alpha = 0.0
            }
        }
    }
}

